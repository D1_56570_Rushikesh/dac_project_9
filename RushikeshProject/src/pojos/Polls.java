package pojos;

import java.sql.Date;

public class Polls {

	private int id;
	private String title;
	private Date startDateTime;
	private Date endDateTime;
	private int createdBy;
	
	public Polls() {
		// TODO Auto-generated constructor stub
	}

	public Polls(int id, String title, Date startDateTime, Date endDateTime, int createdBy) {
		super();
		this.id = id;
		this.title = title;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.createdBy = createdBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "Polls [id=" + id + ", title=" + title + ", startDateTime=" + startDateTime + ", endDateTime="
				+ endDateTime + ", createdBy=" + createdBy + "]";
	}
	
	
}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Answer;
import pojos.Answer2;
import pojos.Polls;

import static utils.DBUtils.fetchConnection;

public class VotesDaoImpl implements IVotesDao {
	
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4,pst5;

	public VotesDaoImpl() throws ClassNotFoundException, SQLException {
		cn = fetchConnection();
		pst2 = cn.prepareStatement("DELETE FROM votes WHERE que_id = ?");
		pst1 = cn.prepareStatement("insert into votes values(default,?,?,?,curdate())");
		pst3 = cn.prepareStatement("with abc as (select selected_option from questions q inner join votes v on q.id = v.que_id where q.poll_id = ?) select abc.selected_option, count(abc.selected_option)/(select count(abc.selected_option) from abc) * 100 from abc group by abc.selected_option order by abc.selected_option");
		pst4 = cn.prepareStatement("select v.selected_option, u.name from questions q inner join votes v on q.id = v.que_id inner join users u on u.id = v.user_id where q.poll_id = ?");
	    pst5 = cn.prepareStatement("select * from votes where user_id = ? and que_id in (select id from questions where poll_id = ?)");
	}
	
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
	}

	public int deleteVotesForQuestionId(int questionId) throws SQLException {

		pst2.setInt(1, questionId);
		int deleteCount = pst2.executeUpdate();
		return deleteCount;
	}
	
	@Override
	public String VoteForLivePollAfterDisplayingOptions(int quesId, String selectedOption, int userId) throws SQLException {
		pst1.setInt(1, quesId);
		pst1.setInt(2, userId);
		pst1.setString(3, selectedOption);
		int updateCount =  pst1.executeUpdate();
		if(updateCount == 1)
			return "answer saved for question id "+quesId; 
		return "failed to save answer for question id "+quesId;
	}

	public List<Answer> percentageVotes(int pollId2) throws SQLException {
		List<Answer> answers = new ArrayList<Answer>();
		pst3.setInt(1, pollId2);
		
		try (ResultSet rst = pst3.executeQuery()) {
			while (rst.next())
				answers.add(new Answer(rst.getString(1),rst.getInt(2)));

		} 
		return answers;

	}
	
	public List<Answer2> userVotes(int pollId) throws SQLException{
		
		List<Answer2> answers = new ArrayList<Answer2>();
		pst4.setInt(1, pollId);
		
		try (ResultSet rst = pst4.executeQuery()) {
			while (rst.next())
				answers.add(new Answer2(rst.getString(1),rst.getString(2)));

		} 
		return answers;
	}

	public boolean checkIfVoted(int id, int pollId3) throws SQLException {
		pst5.setInt(1, id);
		pst5.setInt(2, pollId3);
		try (ResultSet rst = pst5.executeQuery()) {
			while (rst.next())
				return true;

		}
		return false;
	}
}

package pojos;

import java.sql.Date;

public class Votes {

	private int id;
	private int queId;
	private int userId;
	private String selectedOption;
	private Date answeredDateTime;
	
	
	public Votes() {
		// TODO Auto-generated constructor stub
	}
	
	public Votes(int id, int queId, int userId, String selectedOption, Date answeredDateTime) {
		super();
		this.id = id;
		this.queId = queId;
		this.userId = userId;
		this.selectedOption = selectedOption;
		this.answeredDateTime = answeredDateTime;
	}


	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public int getQueId() {
		return queId;
	}


	public void setQueId(int queId) {
		this.queId = queId;
	}

	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	public Date getAnsweredDateTime() {
		return answeredDateTime;
	}

	public void setAnsweredDateTime(Date answeredDateTime) {
		this.answeredDateTime = answeredDateTime;
	}


	@Override
	public String toString() {
		return "Votes [id=" + id + ", queId=" + queId + ", userId=" + userId + ", selectedOption=" + selectedOption
				+ ", answeredDateTime=" + answeredDateTime + "]";
	}
	
	
}

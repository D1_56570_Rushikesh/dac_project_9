package pojos;

public class Answer {

	private String option;
	private int count;
	
	public Answer() {
		// TODO Auto-generated constructor stub
	}

	public Answer(String option, int count) {
		super();
		this.option = option;
		this.count = count;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Answer [option=" + option + ", percentage=" + count + "]";
	}
	
	
}

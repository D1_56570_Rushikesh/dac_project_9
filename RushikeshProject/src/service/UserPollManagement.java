package service;

import java.sql.SQLException;

import dao.QuestionDaoImpl;
import dao.VotesDaoImpl;
import pojos.Questions;

public class UserPollManagement {

	private VotesDaoImpl votesDao;
	private QuestionDaoImpl questionDao;
	
	public UserPollManagement() throws ClassNotFoundException, SQLException {
		votesDao = new VotesDaoImpl();
		questionDao = new QuestionDaoImpl();
	}
	
	public String deleteQuestion(int questionId) throws SQLException {
		int voteDeleteCount = votesDao.deleteVotesForQuestionId(questionId);
		
			int questionDeleteCount = questionDao.deleteQuestionById(questionId);
			if (questionDeleteCount == 1)
				return "Question deleted succesfully.";
		
		return "Question could not be deleted.";
	}
	
	public Questions getQuestion(int questionId) throws SQLException {
		return questionDao.findQuestionsById(questionId);
	}
	
	public String editQuestion(Questions question) throws SQLException {
		int updateCount = questionDao.editQuestion(question);
		if (updateCount == 1)
			return "Question edited successfully.";
		return "Question could not be edited.";
	}
}

package dao;

import java.sql.SQLException;
import java.util.List;

import pojos.Questions;

public interface IQuestionsDao {

	String addQuestion(Questions q1) throws SQLException;

	List<Questions> voteForLivePoll(int pollId) throws SQLException;

}

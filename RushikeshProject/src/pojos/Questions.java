package pojos;

public class Questions {

	private int id;
	private String questionText;
	private String optionA;
	private String optionB;
	private String optionC;
	private String optionD;
	private int pollId;
	private int questionSerialNo;
	
	public Questions() {
		// TODO Auto-generated constructor stub
	}

	public Questions(int id, String questionText, String optionA, String optionB, String optionC, String optionD,
			int pollId, int questionSerialNo) {
		
		this.id = id;
		this.questionText = questionText;
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
		this.pollId = pollId;
		this.questionSerialNo = questionSerialNo;
	}
	
	public Questions(String questionText, String optionA, String optionB, String optionC, String optionD) {
	
		this.questionText = questionText;
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getOptionA() {
		return optionA;
	}

	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}

	public String getOptionB() {
		return optionB;
	}

	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}

	public String getOptionC() {
		return optionC;
	}

	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}

	public String getOptionD() {
		return optionD;
	}

	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}

	public int getPollId() {
		return pollId;
	}

	public void setPollId(int pollId) {
		this.pollId = pollId;
	}

	public int getQuestionSerialNo() {
		return questionSerialNo;
	}

	public void setQuestionSerialNo(int questionSerialNo) {
		this.questionSerialNo = questionSerialNo;
	}

	@Override
	public String toString() {
		return "Questions [id=" + id + ", questionText=" + questionText + ", optionA=" + optionA + ", optionB="
				+ optionB + ", optionC=" + optionC + ", optionD=" + optionD + ", pollId=" + pollId
				+ ", questionSerialNo=" + questionSerialNo + "]";
	}
	
	
}

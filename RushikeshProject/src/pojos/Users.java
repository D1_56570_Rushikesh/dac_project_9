package pojos;

import java.sql.Date;

public class Users {

	private int id;
	private String email;
	private String password;
	private String mobile;
	private String name;
	private String gender;
	private String address;
	private Date birthDate;
	
	public Users() {
		// TODO Auto-generated constructor stub
	}

	public Users(int id, String email, String password, String mobile, String name, String gender, String address,
			Date birthDate) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.birthDate = birthDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", email=" + email + ", password=" + password + ", mobile=" + mobile + ", name="
				+ name + ", gender=" + gender + ", address=" + address + ", birthDate=" + birthDate + "]";
	}
	
	
}

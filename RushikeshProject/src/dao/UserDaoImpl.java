package dao;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Users;

public class UserDaoImpl implements IUserDao{
	
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4;

	public UserDaoImpl() throws ClassNotFoundException, SQLException {
		cn = fetchConnection();
		pst2 = cn.prepareStatement("insert into users values(default,?,?,?,?,?,?,?)");
		pst1 = cn.prepareStatement("select * from users where email = ?");
		pst3 = cn.prepareStatement("UPDATE users SET mobile = ?, address = ?, birth_date = ? WHERE id = ?");
		pst4 = cn.prepareStatement("UPDATE users SET password = ? WHERE id = ?");

	}
	
	
	@Override
	public String addNewUser(Users newUser) throws SQLException {
		
		pst2.setString(1, newUser.getEmail());
		pst2.setString(2, newUser.getPassword());
		pst2.setString(3, newUser.getMobile());
		pst2.setString(4, newUser.getName());
		pst2.setString(5, newUser.getGender());
		pst2.setString(6, newUser.getAddress());
		pst2.setDate(7, newUser.getBirthDate());
		
		
		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "New user details added....";

		return "Adding new user failed...";
	}

	
	public List<Users> findUsers(String email) throws SQLException {
		List<Users> users = new ArrayList<Users>();
		pst1.setString(1, email);
		
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				users.add(new Users(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7),rst.getDate(8)));

		} 
		
		return users;
	}

	@Override
	public Users validateUser(String email, String password) throws SQLException {
		List<Users> findUsers = findUsers(email);
		if(findUsers == null) {
			return null;
		}else {
			Users user = findUsers.get(0);
			if(user.getPassword().equals(password)) {
				return user;
			}
		}
		return null;
	}
	
	public int updateUserById(String mobile, String address, Date birthDate, int userId) throws SQLException {
		pst3.setString(1, mobile);
		pst3.setString(2, address);
		pst3.setDate(3, birthDate);
		pst3.setInt(4, userId);
		int updateCount = pst3.executeUpdate();
		return updateCount;
	}
	
	public int updatePasswordById(String password, int UserId) throws SQLException {
		pst4.setString(1, password);
		pst4.setInt(2, UserId);
		int updateCount = pst4.executeUpdate();
		return updateCount;
	}

}

package dao;

import java.sql.SQLException;

import pojos.Users;

public interface IUserDao {

	String addNewUser(Users newUser) throws SQLException;

	Users validateUser(String email, String password) throws SQLException;

}

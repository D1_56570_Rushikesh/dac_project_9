package dao;

import java.sql.SQLException;

public interface IVotesDao {


	String VoteForLivePollAfterDisplayingOptions(int quesId, String selectedOption, int userId) throws SQLException;

}

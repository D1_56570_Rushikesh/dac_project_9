package service;

import java.sql.Date;
import java.sql.SQLException;
import dao.UserDaoImpl;


public class UserAccountManagement {

	private UserDaoImpl userDao;
	
	public UserAccountManagement() throws ClassNotFoundException, SQLException {
		userDao = new UserDaoImpl();
	}
	
	public String editProfile(String mobile, String address, Date birthDate, int userId) throws SQLException {
		int updateCount = userDao.updateUserById(mobile, address, birthDate, userId);
		if (updateCount == 1) {
			return "User details updated successfully.";
		}
		return "User details could not be updated.";
	}
	
	public String updatePassword(String password, int userId) throws SQLException {
		int updateCount = userDao.updatePasswordById(password, userId);
		if (updateCount == 1)
			return "Password changed successfully.";
		return "Password could not be updated.";
	}
}

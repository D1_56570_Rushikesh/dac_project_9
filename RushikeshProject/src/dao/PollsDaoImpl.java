package dao;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Polls;

public class PollsDaoImpl implements IPollsDao {

	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4,pst5;

	public PollsDaoImpl() throws ClassNotFoundException, SQLException {

		cn = fetchConnection();
		String sql = "select * from polls where created_by = ?";
		pst1 = cn.prepareStatement(sql);
		pst2 = cn.prepareStatement("insert into polls values(default,?,?,?,?)");
		pst3 = cn.prepareStatement("update polls set title=?,start_datetime = ?,end_datetime = ? where id=?");
		
		pst4 = cn.prepareStatement("select * from polls where start_datetime >= ? and end_datetime <= ?");
		pst5 = cn.prepareStatement("select * from polls where end_datetime < curdate()");


	}
	
	@Override
	public List<Polls> getMyPolls(int id) throws SQLException {
		
		List<Polls> polls = new ArrayList<>();
		pst1.setInt(1, id);
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				polls.add(new Polls(rst.getInt(1), rst.getString(2), rst.getDate(3), rst.getDate(4),rst.getInt(5)));

		} 
		return polls;
	}
	
	@Override
	public String addPoll(Polls newPoll) throws SQLException {
		
		pst2.setString(1, newPoll.getTitle());
		pst2.setDate(2, newPoll.getStartDateTime());
		pst2.setDate(3, newPoll.getEndDateTime());
		pst2.setInt(4, newPoll.getCreatedBy());
		
		
		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "Poll details added....";

		return "Adding new poll failed...";
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("emp dao cleaned up !");
	}
	
	
	@Override
	public String updatePoll(String title1, Date strtdate1, Date endDate1, int id)  throws SQLException {
		
		pst3.setString(1, title1);
		pst3.setDate(2, strtdate1);
		pst3.setDate(3, endDate1);
		pst3.setInt(4, id);
		
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "Poll details updated....";
		
		return "Updating poll details failed...";
	}
	
	@Override
	public List<Polls> getListOfLivePolls(Date startDateTime, Date endDateTime) throws SQLException {
		List<Polls> polls = new ArrayList<>();
		pst4.setDate(1, startDateTime);
		pst4.setDate(2, endDateTime);
		try(ResultSet rst = pst4.executeQuery())
		{
			while(rst.next())
				polls.add(new Polls(rst.getInt(1), rst.getString(2), rst.getDate(3), rst.getDate(4), rst.getInt(5)));
			
		}
		return polls;
	}
	@Override
	public List<Polls> getListOfOldPolls() throws SQLException {
		List<Polls> polls = new ArrayList<>();
		try(ResultSet rst = pst5.executeQuery())
		{
			while(rst.next())
				polls.add(new Polls(rst.getInt(1), rst.getString(2), rst.getDate(3), rst.getDate(4), rst.getInt(5)));
		}
		return polls;
	}
}

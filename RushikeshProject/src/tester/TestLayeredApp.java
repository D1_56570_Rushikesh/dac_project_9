package tester;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;
import dao.PollsDaoImpl;
import dao.QuestionDaoImpl;
import dao.UserDaoImpl;
import dao.VotesDaoImpl;
import pojos.Answer;
import pojos.Answer2;
import pojos.Polls;
import pojos.Questions;
import pojos.Users;
import service.UserAccountManagement;
import service.UserPollManagement;

public class TestLayeredApp {

	public static boolean exit = false;
	
	
	public static void main(String[] args) {
		
		try (Scanner sc = new Scanner(System.in);
				Scanner ph = new Scanner(System.in);
				Scanner sc2 = new Scanner(System.in)) {
			Users loggedInUser = null;
			UserPollManagement userPoll = new UserPollManagement();
			UserAccountManagement uam = new UserAccountManagement();
			PollsDaoImpl dao = new PollsDaoImpl();
			QuestionDaoImpl dao1 = new QuestionDaoImpl();
			UserDaoImpl dao2 = new UserDaoImpl();
			VotesDaoImpl dao3 = new VotesDaoImpl();
			

			
			while(!exit) {

				System.out.println("Enter your choice:");
				System.out.println("1. Sign Up");
				System.out.println("2. Sign In");
				System.out.println("3. Edit profile");
				System.out.println("4. Change password");
				System.out.println("5. Sign Out");
				System.out.println("6. Show my polls ");
				System.out.println("7. Create new poll ");
				System.out.println("8. Edit a poll");
				System.out.println("9. Add question to poll");
				System.out.println("10. Delete question from poll.");
				System.out.println("11. Edit question in poll.");
				System.out.println("12. View poll result summary.");
				System.out.println("13. View poll result details.");
				System.out.println("14. List live polls");
				System.out.println("15. Vote for a live poll.");
				System.out.println("16. View poll result summary(for voted polls only).");
				System.out.println("17. List old(Archive) polls");
				System.out.println("18. View old poll result summary.");
				System.out.println("19. Exit");
				System.out.println("(D1_PG-DAC_DAC_PROJECT_9) ");
				
					int choice = sc.nextInt();
					switch (choice) {
					case 1:
						System.out.println("Enter your details one by one... email, password, mobile, name, gender, address, birth_date ");
						
						Users newUser = new Users(0,sc.next(),sc.next(),sc.next(),sc.next(),sc.next(),sc.next(),Date.valueOf(sc.next()));
						System.out.println(dao2.addNewUser(newUser));
						break;
						
					case 2:
						System.out.println("Enter your email..");
						String email = sc.next();
						System.out.println("Enter your password..");
						String password = sc.next();
						
						Users authenticatedUser = dao2.validateUser(email,password);
						if(authenticatedUser != null) {
							loggedInUser = authenticatedUser;
							System.out.println("Logged in successfully...");
						}else {
							System.out.println("Enter correct data...");
						}
						break;
						
					case 3:
						
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
							System.out.println("Enter mobile no");
							String mobileNo = sc.next();
							System.out.println("Enter address");
							String address = sc.next();
							System.out.println("Enter birth date(yyyy-mm-dd)");
							Date birthDate = Date.valueOf(sc.next());
							System.out.println(uam.editProfile(mobileNo, address, birthDate, loggedInUser.getId()));
						}
						break;
						
					case 4:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter new password.");
						String password2 = sc.next();
						System.out.println(uam.updatePassword(password2, loggedInUser.getId()));
						}
						break;

					case 5:
						
						System.out.println("Thank you !!!" + loggedInUser.getName());
						loggedInUser = null;
						break;
						
					case 6:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						dao.getMyPolls(loggedInUser.getId()).forEach(System.out::println);
						
						}
						break;
						
					case 7:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll details...");
						System.out.println("Enter Title");
						String title = sc.next();
						System.out.println("Enter start date");
						Date strtdate = Date.valueOf(sc.next());
						System.out.println("Enter end date");
						Date endDate = Date.valueOf(sc.next());
						
						Polls poll = new Polls(0,title,strtdate,endDate,loggedInUser.getId());
						System.out.println(dao.addPoll(poll));
						}
						break;
						
					case 8:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll details...");
						System.out.println("Enter id..");
						int id = sc.nextInt();
						System.out.println("Enter Title");
						String title1 = sc.next();
						System.out.println("Enter start date");
						Date strtdate1 = Date.valueOf(sc.next());
						System.out.println("Enter end date");
						Date endDate1 = Date.valueOf(sc.next());
						
						System.out.println(dao.updatePoll(title1,strtdate1,endDate1,id));
						}
						break;
						
					case 9:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter question details...");
						System.out.println("Enter poll id..");
						int pollId = sc.nextInt();
						System.out.println("Enter question text");
						String ques = ph.nextLine();
						System.out.println("options and question serial number (in the poll)");
						
						Questions q1 = new Questions(0,ques,sc.next(),sc.next(),sc.next(),sc.next(),pollId,sc.nextInt());
						System.out.println(dao1.addQuestion(q1));
						}
						break;
						
					case 10:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter question id");
						int questionId = sc.nextInt();
						System.out.println(userPoll.deleteQuestion(questionId));
						}
						break;
						
					case 11:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter question id.");
						int questionId = sc.nextInt();
						Questions question = userPoll.getQuestion(questionId);
						System.out.println("Enter new question text.");
						String questionText = sc2.nextLine();
						question.setQuestionText(questionText);
						System.out.println("Enter new question serial number.");
						int questionSerialNo = sc.nextInt();
						question.setQuestionSerialNo(questionSerialNo);
						System.out.println(userPoll.editQuestion(question));
						}
						break;

					case 12:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll id.");
						int pollId2 = sc.nextInt();
						// Display percentage votes for option A, B, C, and D for the given poll id.
						 List <Answer> answers = dao3.percentageVotes(pollId2);
						 answers.forEach(System.out::println);
						}
						break;
						
					case 13:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll id.");
						int pollId = sc.nextInt();
						List <Answer2> answers2 = dao3.userVotes(pollId);
						 answers2.forEach(System.out::println);
						}
						break;
						
					case 14:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter start date and end date in year-month-date:  ");
						dao.getListOfLivePolls(Date.valueOf(sc.next()), Date.valueOf(sc.next())).forEach(System.out::println);
						System.out.println("List Of Live Polls...");
						}
						break;
						
						
					case 15:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll id: ");
						dao1.voteForLivePoll(sc.nextInt()).forEach(System.out::println);
						System.out.println("Enter question id and option: ");
						System.out.println(dao3.VoteForLivePollAfterDisplayingOptions(sc.nextInt(), sc.next(),loggedInUser.getId()));
						}
						
					break;
					
					case 16:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll id.");
						
						int pollId3 = sc.nextInt();
						
						boolean ok = dao3.checkIfVoted(loggedInUser.getId(),pollId3);
						
						if(ok) {
							List <Answer> answers3 = dao3.percentageVotes(pollId3);
							 answers3.forEach(System.out::println);
						}else {
							System.out.println("You haven't voted for this...");
						}
						}
						 
						 break;
					
					case 17:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("List of Old (Archived) Polls: ");
						dao.getListOfOldPolls().forEach(System.out::println);
						}
						break;
						
					case 18:
						if(loggedInUser == null) {
							System.out.println("Login first..");
						}else {
						System.out.println("Enter poll id.");
						int pollId3 = sc.nextInt();
						
						
						 List <Answer> answers3 = dao3.percentageVotes(pollId3);
						 answers3.forEach(System.out::println);
						}
						 break;
						
					case 19:
						exit = true;
						break;
						
					default:
						System.out.println("Invalid choice");
						break;
					}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

	



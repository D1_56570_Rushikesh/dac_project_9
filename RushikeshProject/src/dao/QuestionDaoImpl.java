package dao;

import static utils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Questions;

public class QuestionDaoImpl implements IQuestionsDao{

	private Connection cn;
	private PreparedStatement pst1, pst2, pst3,pst4,pst5;

	public QuestionDaoImpl() throws ClassNotFoundException, SQLException {
		cn = fetchConnection();
		pst4 = cn.prepareStatement("select * from questions where poll_id=?");
		pst1 = cn.prepareStatement("DELETE FROM questions WHERE id = ?");
		pst2 = cn.prepareStatement("SELECT * FROM questions WHERE id = ?");
		pst3 = cn.prepareStatement("UPDATE questions SET question_text = ?, question_serial_no = ? WHERE id = ?");
		pst5 = cn.prepareStatement("insert into questions values(default,?,?,?,?,?,?,?)");


	}
	
	
	@Override
	public String addQuestion(Questions q1) throws SQLException{
		
		pst5.setString(1, q1.getQuestionText());
		pst5.setString(2, q1.getOptionA());
		pst5.setString(3, q1.getOptionB());
		pst5.setString(4, q1.getOptionC());
		pst5.setString(5, q1.getOptionD());
		pst5.setInt(6, q1.getPollId());
		pst5.setInt(7, q1.getQuestionSerialNo());
		
		
		int updateCount = pst5.executeUpdate();
		if (updateCount == 1)
			return "Question details added....";

		return "Adding new question failed...";
	}


	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("emp dao cleaned up !");
	}
	
		
		public int deleteQuestionById(int questionId) throws SQLException {
			pst1.setInt(1, questionId);
			int deleteCount = pst1.executeUpdate();
			return deleteCount;
		}
		
		public Questions findQuestionsById(int questionId) throws SQLException {
			pst2.setInt(1, questionId);
			ResultSet result = pst2.executeQuery();
			
			if (result.next()) {
				return new Questions(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5), result.getString(6), result.getInt(7), result.getInt(8));
			}
			
			return null;
		}
		
		public int editQuestion(Questions question) throws SQLException {
			pst3.setString(1, question.getQuestionText());
			pst3.setInt(2, question.getQuestionSerialNo());
			pst3.setInt(3, question.getId());
			int updateCount = pst3.executeUpdate();
			return updateCount;
		}
		
		@Override
		public List<Questions> voteForLivePoll(int pollId) throws SQLException {
			List<Questions> votes = new ArrayList<>();
			pst4.setInt(1, pollId);
			try(ResultSet rst = pst4.executeQuery())
			{
				while(rst.next())
					votes.add(new Questions(rst.getInt(1),rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6),rst.getInt(7),rst.getInt(8)));
			}
			return votes;
		}
}

package pojos;

public class Answer2 {

	
	private String userName;
	private String option;
	
	public Answer2() {
		
	}

	public Answer2(String userName, String option) {
		super();
		this.userName = userName;
		this.option = option;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	@Override
	public String toString() {
		return "Answer2 [option =" + userName + ", userName =" + option + "]";
	}


	
	
}

package dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import pojos.Polls;

public interface IPollsDao {

	List<Polls> getMyPolls(int id) throws SQLException;

	String addPoll(Polls newPoll) throws SQLException;

	String updatePoll(String title1, Date strtdate1, Date endDate1, int id) throws SQLException;

	List<Polls> getListOfOldPolls() throws SQLException;

	List<Polls> getListOfLivePolls(Date startDateTime, Date endDateTime) throws SQLException;

}
